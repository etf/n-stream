Summary: Streaming support for Nagios
Name: nagios-stream
Version: 0.1.22
Release: 2%{?dist}
Source: %{name}-%{version}.tar.gz
License: ASL 2.0
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Requires: python3-messaging 
Requires: python3-dirq
Url: https://gitlab.cern.ch/etf/n-stream
BuildRequires: python3-setuptools 
BuildRequires: /usr/bin/pathfix.py

%description
Streaming support for Nagios

%prep
%setup -n %{name}-%{unmangled_version} -n %{name}-%{version}
pathfix.py -pni "%{__python3} %{py3_shbang_opts}" . bin/mq_handler bin/ocsp_handler

%build
%py3_build

%install
%{__python3} setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ -f /usr/bin/disable_nstream ]; then
    chmod 755 /usr/bin/disable_nstream
fi

if [ -f /usr/bin/enable_nstream ]; then
    chmod 755 /usr/bin/enable_nstream
fi

if [ -f /usr/bin/mq_handler ]; then
    chmod 755 /usr/bin/mq_handler
fi

if [ -f /usr/bin/ocsp_handler ]; then
    chmod 755 /usr/bin/mq_handler
fi


%files -f INSTALLED_FILES
%defattr(-,root,root)
%doc README.md
