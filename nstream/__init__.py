AUTHOR = "Marian Babik"
AUTHOR_EMAIL = "<marian.babik@cern.ch>"
COPYRIGHT = "Copyright (C) 2015 CERN"
VERSION = "0.1.22"
DATE = "23 Jun 2015"
__author__ = AUTHOR
__version__ = VERSION
__date__ = DATE
